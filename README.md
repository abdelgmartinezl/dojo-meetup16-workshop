# Microservicios en Docker
Paso a paso para montar un microservicio, construido sobre Flask, sobre 
contenedores Docker.

## Concepto: Monolítico

![Monolítico]( https://dojo16.s3.amazonaws.com/img/monolithic.png  "Monolítico")

Arquitectura que hace referencia a una aplicación en la que la capa de interfaz 
de usuario y la capa de acceso de datos están combinadas en un mismo programa 
y sobre una misma plataforma.

## Concepto: Microservicios

![Microservicios]( https://dojo16.s3.amazonaws.com/img/microservices.png  "Microservicios")

Arquitectura que estructura una aplicación como una colección de servicios, que
ejecutan sus propios procesos y se comunican mediante mecanismos ligeros (APIs).

### Características
* Débilmente acoplado
* Despliegue independiente
* Fácil mantenimiento y pruebas
* Organizado alredador del negocio
* Administrado por equipos pequeños
* Flexibiliad en el uso de tecnologías
* Entrega rápida, frecuente y confiable

### Metodología
Para aplicar esta arquitectura, se sugiera la ejecución de los siguientes pasos:
1. Entender los objetivos del negocio
2. Asegurar que todos entiendan la tarea
3. Elegir el lenguaje de programación
4. Considerar el almacenamiento de datos
5. Utilizar metodologías ágiles
6. Monitorizar constantemente los servicios

## Concepto: Contenedores
Es una unidad de software que empaqueta código y todas sus dependencias para 
correr rápida y confiablemente de un entorno a otro.

![Contenedores]( https://dojo16.s3.amazonaws.com/img/containers.png  "Contenedores")

Los contenedores aíslan el software de su entorno y aseguran que trabaje 
uniformemente a pesar de las diferencias entre ambientes.

### Docker

![Docker]( https://dojo16.s3.amazonaws.com/img/docker.png  "Docker")

Es un conjunto de plataformas como servicio que está diseñada para hacer más
fácil la creación, despliegue y ejecución de aplicaciones a través del uso de
contenedores. Esto permite al desarrollador empaquetar una aplicación con todas
sus partes necesarias, tales como librerías y otras dependencias y entregarlas
en un único paquete. Haciendo esto, se asegura que la aplicación se pueda
ejecutar en cualquier otro servidor, on-premise o nube, sin necesitar de una
configuración personalizada.

### Kubernetes

![Kubernetes]( https://dojo16.s3.amazonaws.com/img/kubernetes.png  "Kubernetes")

Es un sistema de orquestación de contenedores que permite la automatización del
despliegue, escalabilidad y gestión. Orquesta la infraestructura de cómputo, 
redes y almacenamiento para que las cargas de trabajo de usuario no tengan que 
hacerlo. Entre sus principales características se incluye:
* Descubrimiento y balanceo de cargas de servicios
* Orquestación de almacenamiento
* Automatización de rollouts y rollbacks
* Automatización de empaquetado de binarios
* Self-healing
* Configuración y gestión de secretos (información sensitiva)

## Taller
### Pre-requisitos
1. Instalar [Docker Desktop](https://www.docker.com/products/docker-desktop)
2. Habilitar Kubernetes en el Docker Desktop
3. Instalar [Python 3](https://www.python.org/downloads/)

### Prueba de Aplicación Sin Contenedores

1. Clonar el proyecto:
```bash
  git clone https://gitlab.com/abdelgmartinezl/dojo-meetup16-workshop.git
  cd dojo-meetup16-workshop
```

2. Instalar los paquetes:
```bash 
  pip install -r requirements.txt
```

3. Iniciar el servicio: 
```bash
  $ python main.py
```

### Contenerización de Aplicación

1. Desde el directorio del repositorio, construir la imagen:
```bash
  docker build -f Dockerfile -t dojo-meetup16:latest .
```

### Prueba de Aplicación En Contenedores

1. Ejecutar un contenedor de la imagen recién construida:
```bash 
  docker run -p 5000:5000 -d dojomeetup16:latest
```

2. Detener y eliminar el contenedor ejecutado (debe validar primero el Container ID): 
```bash
  docker ps
  docker stop 7f01e2fb8923114ca26e721af8811623c4b5922d95b9ff8dad197daaf60252d8
  docker rm 7f01e2fb8923114ca26e721af8811623c4b5922d95b9ff8dad197daaf60252d8
```

### Creación de Servicio en Kubernetes

1. Crear el Deployment basado en la plantilla YAML:
```bash 
  kubectl apply -f deployment.yaml
```

2. Crear el Service basado en la plantilla YAML:
```bash
  kubectl apply -f service.yaml
```

3. Validar que el estado del Pod:
```bash
  kubectl get pods
```

4. Ver cuál es el puerto que se expuso en el alcance NodePort:
```bash
  kubectl get service movies
```

### Prueba de Servicio en Kubernetes

1. Validar que nuestro microservicio, basado en Flask, se esté ejecutando:
```bash
  curl localhost:31843
  curl localhost:31843/api/movies
```

### Escalar Servicio en Kubernetes

1. Crecer nuestro servicio manualmente a 4 réplicas:
```bash
  kubectl scale --replicas=4 deployment movies-deployment
```

2. Achicar nuestro servicio manualmente a 2 réplicas:
```bash
  kubectl scale --replicas=2 deployment movies-deployment
```

3. Aplicar política Horizontal Pod Autoscaler:
```bash
  kubectl autoscale deployment.v1.apps/movies-deployment --min=2 --max=4 --cpu-percent=20
```

4. Eliminar política Horizontal Pod Autoscaler:
```bash
  kubectl delete hpa movies-deployment
```

### Modificación de Imagen en Kubernetes

1. Cambiar imagen en caliente (intencionalmente generé una imagen que tiene un error):
```bash
  kubectl set image deployment movies-deployment movies=dojo-meetup16:2
```

1. Cambiar imagen en caliente (volvemos a la imagen original):
```bash
  kubectl set image deployment movies-deployment movies=dojo-meetup16:1
```

### Eliminación de Servicio en Kubernetes

1. Detener y eliminar la ejecución de nuestro microservicio:
```bash
  kubectl delete services movies
  kubectl delete deployment movies-deployment
```